/*
 * Task IPK - webclient: Download client in C++ for IPK 2015/2016
 * Name and surname: Tomáš Polášek
 * Login: xpolas34
 */

#include "HttpReceiver.h"

namespace app
{
    HttpReceiver::HttpReceiver(const std::string &inputUrl)
    {
        int redirects = 0;
        std::string url = inputUrl;
        bool useOldHttp = false;
        while (redirects < MAX_REDIRECTS)
        {
            Status code = run(url, useOldHttp);
            if (code == Status::OK)
                break;
            else if (code == Status::USE_OLD_HTTP)
                useOldHttp = true;
            else
            {
                useOldHttp = false;
                redirects++;
            }
        }

        if (redirects == MAX_REDIRECTS)
            err::exit(err::Error::MESSAGE, "Too many redirects!");
    }

    app::Status HttpReceiver::run(std::string &inputUrl, bool useOldHttp)
    {
        // Parse the input URL.
        util::UrlParser urlParser(inputUrl);

        // Open local socket.
        net::TcpSocket tcpSocket;

        // Get the server information.
        net::HostInfo hostInfo(urlParser);

        // Connect to the server
        net::TcpConnection connection(hostInfo, urlParser);
        connection.connect(tcpSocket);

        // Compile the message
        std::stringstream messageStream;
        if (!useOldHttp)
        {
            messageStream << "GEeT " << urlParser.getPath()
                          << " HTTP/1.1\r\nHOST: " << urlParser.getServer()
                          << "\r\nUser-agent: webclient\r\n"
                          << "Connection: close\r\n\r\n";
        }
        else
        {
            messageStream << "GET " << urlParser.getPath()
                          << " HTTP/1.0\r\nHOST: " << urlParser.getServer()
                          << "\r\nUser-agent: webclient\r\n"
                          << "Connection: close\r\n\r\n";
        }
        std::string message = messageStream.str();

        // Create a sender for our socket.
        net::TcpSender sender(tcpSocket);

        // Send the get request.
        sender.sendMessage(message);

        // Create a receiver for our socket.
        net::TcpReceiver receiver(tcpSocket);

        // Receive the first message.
        std::string received = receiver.receiveMessage();

        // Parse the HTTP header.
        util::HttpParser httpParser(received);

#if 0
        // Print some debug information
        std::cout << httpParser.getVersionString() << std::endl;
        std::cout << httpParser.getCode() << std::endl;
        std::cout << httpParser.getCodeMessage() << std::endl;
        std::cout << httpParser.getLocation() << std::endl;
        std::cout << httpParser.getTypeString() << std::endl;
        std::cout << (httpParser.isChunked() ? "Is chunked" : "Is not chunked") << std::endl;
#endif

        int messageCode = httpParser.getCode();

        // If HTTP/1.1 is not supported
        if (messageCode == 501 || messageCode == 505)
            return Status::USE_OLD_HTTP;

        // If we got redirected, edit the url and return status.
        if (messageCode == 301 || messageCode == 302)
        {
            std::string newUrl = httpParser.getLocation();
            // If the URL is not absolute.
            if (newUrl[0] == '/')
            {
                std::stringstream urlBuilder;
                urlBuilder << urlParser.getProtocol();
                urlBuilder << urlParser.getServer();
                urlBuilder << ":" << urlParser.getPort();
                urlBuilder << newUrl;
                inputUrl = urlBuilder.str();
            }
            else
                inputUrl = httpParser.getLocation();
            return Status::REDIRECT;
        }
        else if (messageCode < 600 && messageCode >= 400)
        {
            std::stringstream ss;
            ss << "Error code : " << messageCode;
            err::exit(err::Error::PARAM, ss.str());
        }

        if (urlParser.getObject() == "webclient")
            err::exit(err::Error::OUTPUT, "Object name cannot be the same as the program name!");
        mOutput.open(urlParser.getObject(), std::ios::out | std::ios::binary);
        if (!mOutput.is_open())
            err::exit(err::Error::OUTPUT, "Unable to open file!");

        if (httpParser.isChunked())
            processChunked(received, httpParser, receiver);
        else
            processNormal(received, httpParser, receiver);

        mOutput.close();

        return Status::OK;
    }

    void HttpReceiver::processChunked(const std::string &begMessage,
                                      const util::HttpParser &parser,
                                      net::TcpReceiver &receiver)
    {
        /*
         * One header,
         * Several chunks,
         * Can be split to multiple receives.
         */

        bool running = true;
        bool skipNext = false;
        bool first = true;
        size_t startIndex = 0;
        std::string received = begMessage;

        // Iterate through all chunks
        while (running)
        {
            //std::cout << "Parsing data size : " <<  startIndex << " " << first << std::endl;

            // Find the data content size.
            size_t dataIndex = 0;
            mCurrDataSize = parseDataSize(received, dataIndex, startIndex, first);
            startIndex = 0;
            first = false;

            /*
            std::cout << "Starting new chunk : " << mCurrDataSize << " " << dataIndex << std::endl;
            if (mCurrDataSize == 0)
                std::getchar();
                */

            // Iterate through all the chunk parts
            while (mCurrDataSize > 0)
            {
                skipNext = false;
                //std::cout << "Chunk data still remaining : " << mCurrDataSize << std::endl;
                // Search for data block end.
                auto begIter = received.cbegin() + dataIndex;
                auto endIter = received.cend();
                std::regex regDataEnd("(\r\n)0\r\n\r\n$");
                std::cmatch result;
                // If the trailing zero is found, it means we got all the data.
                if (std::regex_search(received.c_str(), result, regDataEnd))
                {
                    endIter = received.cbegin() + result.position(1);
                    running = false;
                }

                // Calculate the actual data size inside this part.
                size_t dataSize = endIter - begIter;
                if (dataSize > BUFFER_SIZE)
                    err::exit(err::Error::MESSAGE, "Data segment too long!");

                //std::cout << "Data in this part : " << dataSize << std::endl;

                // End of one chunk
                if (dataSize > mCurrDataSize)
                {
                    //std::cout << "Too much data : \n" << std::string(begIter, endIter) << std::endl;
                    endIter = begIter + mCurrDataSize;
                    startIndex = dataIndex + mCurrDataSize + 2;
                    dataIndex = dataIndex + mCurrDataSize;
                    // If only other data is not the \r\n
                    //std::cout << "Datasize : " << dataSize << " currDataSize : " << mCurrDataSize << std::endl;
                    if (dataSize != mCurrDataSize + 2)
                        skipNext = true;

                    mCurrDataSize = 0;

                    running = true;
                }
                else
                    mCurrDataSize -= dataSize;


                // Process the data in this part.
                //std::copy(begIter, endIter, mData);
                //std::cout << "##################start##########################################################" << std::endl;
                std::string data(begIter, endIter);
                mOutput << data;
                //std::cout << std::string(received.cbegin(), begIter) << std::endl << "From here\n" << data << std::endl;
                //std::cout << data;
                //std::cout << "##################end############################################################" << std::endl;

                /*
                std::cout << "Got data : " << data.length() << std::endl;

                std::cout << "The rest : " << std::endl << std::string(endIter, received.cend()) << std::endl;

                std::cout << running << " " << skipNext << std::endl;
                 */

                if (running && !skipNext)
                {
                    //std::cout << "Getting message" << std::endl;
                    received = receiver.receiveMessage();
                    //std::cout << "Got it" << std::endl;
                    dataIndex = 0;
                    startIndex = 0;
                }
                else
                    skipNext = false;
            }
        }
    }

    void HttpReceiver::processNormal(const std::string &begMessage,
                                     const util::HttpParser &parser,
                                     net::TcpReceiver &receiver)
    {
        // Get the content length.
        std::cmatch result;
        /*
        std::regex regContentLength("Content-Length: (\\d+)\r\n");
        if (!std::regex_search(begMessage.c_str(), result, regContentLength))
            err::exit(err::Error::MESSAGE, "Message is not chunked and is missing Content-Length!");
        std::stringstream lengthStream(result.str(1));
        if (!(lengthStream >> mCurrDataSize))
            err::exit(err::Error::PARAM, "The content length is not convertible!");
            */

        // Find the starting index of data.
        std::regex regContentBeg("^(?:.+\r\n)+(\r\n)");
        std::regex_search(begMessage.c_str(), result, regContentBeg);
        size_t dataIndex = static_cast<size_t>(result.position(1) + 2);

        std::string received = begMessage;

        // Run through all of the messages.
        //while (mCurrDataSize > 0)
        while (received.size())
        {
            //std::cout << "Remaining data : " << mCurrDataSize << std::endl;
            auto begIter = received.cbegin() + dataIndex;
            auto endIter = received.cend();
            /*
            if (dataIndex + mCurrDataSize < received.length())
                endIter = received.cbegin() + dataIndex + mCurrDataSize;
            size_t dataSize = endIter - begIter;
            //std::cout << "Got : " << dataSize << std::endl;
            mCurrDataSize -= dataSize;
             */

            dataIndex = 0;

            std::string data(begIter, endIter);
            mOutput << data;

            received = receiver.receiveMessage();
            /*
            if (mCurrDataSize > 0)
            {
                received = receiver.receiveMessage();
                //std::cout << "New message size : " << received.length() << std::endl;
            }
             */
        }
    }

    size_t HttpReceiver::parseDataSize(const std::string &message, size_t &dataIndex,
                                       size_t startIndex, bool first) const
    {
        std::regex regContentSize;
        if (first)
            regContentSize = std::regex("^(?:.+\r\n)+\r\n([1-9a-f][\\da-f]*)\r\n");
        else
        {
            regContentSize = std::regex("^([1-9a-f][\\da-f]*)\r\n");
        }

        //std::cout << "Parsing : \n" << message.c_str() + startIndex << std::endl;

        std::cmatch result;
        std::regex_search(message.c_str() + startIndex, result, regContentSize);
        //std::cout << "Captured : " << result.str(1) << std::endl;

        // Calculate the index, where data block starts.
        dataIndex = result.position(1) + startIndex + result.str(1).length() + 2;

        //const char *res = result.str(1).c_str();
        //const char *endPtr = res;
        //long int dataSize = strtol(result.str(1).c_str(), &endPtr, 16);
        //size_t pos = 0;
        long int dataSize = strtol(result.str(1).c_str(), nullptr, 16);
        //if (*endPtr != '\0')
        //if (pos != result.str(1).length())
            //err::exit(err::Error::MESSAGE, "Could not convert data length to integer!");

        return static_cast<size_t>(dataSize);
    }

    void HttpReceiver::clearBuffer()
    {
        bzero(mData, BUFFER_SIZE + 1);
    }
}
