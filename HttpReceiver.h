/*
 * Task IPK - webclient: Download client in C++ for IPK 2015/2016
 * Name and surname: Tomáš Polášek
 * Login: xpolas34
 */

#ifndef HTTPRECEIVER_H
#define HTTPRECEIVER_H

#include <iostream>
#include <string>
#include <regex>
#include <fstream>

#include "NetworkIncludes.h"

#include "ErrorHandling.h"
#include "UrlParser.h"
#include "TcpSocket.h"
#include "HostInfo.h"
#include "TcpConnection.h"
#include "TcpSender.h"
#include "TcpReceiver.h"
#include "HttpParser.h"

namespace app
{

    enum class Status
    {
        OK = 0,
        REDIRECT,
        USE_OLD_HTTP,
        UNKNOWN
    };

    /**
     * The main application.
     */
    class HttpReceiver
    {
    public:
        HttpReceiver() = delete;

        HttpReceiver(const std::string &inputUrl);

        static const size_t BUFFER_SIZE = net::TcpReceiver::BUFFER_SIZE;
        static const int MAX_REDIRECTS = 5;
    private:
        /**
         * Find and parse the data size.
         *
         * @param message Message to parse.
         * @param dataIndex Used to return the index.
         * @param startIndex Where to start searching for data block.
         * @param first If true, the message is parsed as the first HTTP message.
         * @return Returns the size of data segment.
         */
        size_t parseDataSize(const std::string &message, size_t &dataIndex,
                             size_t startIndex, bool first) const;

        void processChunked(const std::string &begMessage,
                            const util::HttpParser &parser,
                            net::TcpReceiver &receiver);
        void processNormal(const std::string &begMessage,
                           const util::HttpParser &parser,
                           net::TcpReceiver &receiver);

        app::Status run(std::string &inputUrl, bool useOldHttp);

        void clearBuffer();

        size_t mCurrDataSize;
        size_t mDataSize;
        std::ofstream mOutput;

        char mData[BUFFER_SIZE + 1];
    protected:
    };
}


#endif //HTTPRECEIVER_H
