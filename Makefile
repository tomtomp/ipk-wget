CC=gcc
CXX=g++
RM=rm -f
CPPFLAGS=-std=c++14
LDLIBS=-static-libstdc++

SRC=main.cpp ErrorHandling.cpp TcpConnection.cpp TcpReceiver.cpp TcpSender.cpp TcpSocket.cpp UrlParser.cpp HostInfo.cpp HttpParser.cpp HttpReceiver.cpp

all: webclient

webclient: $(SRC)
	$(CXX) $(CPPFLAGS) -o webclient $(SRC) $(LDLIBS)
