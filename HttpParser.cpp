/*
 * Task IPK - webclient: Download client in C++ for IPK 2015/2016
 * Name and surname: Tomáš Polášek
 * Login: xpolas34
 */

#include "HttpParser.h"

namespace util
{
    HttpParser::HttpParser(const std::string &message) :
        mHttpVersion(HttpVersion::HTTP1_0),
        mCode(0),
        mCodeMessage(""),
        mLocation(""),
        mChunked(false),
        mContentType(HttpContentType::TEXT_HTML),
        mCurrDataSize(0),
        mContentLength(0)
    {
        parse(message);
    }

    void HttpParser::parse(const std::string &message)
    {
        // Get the HTTP version and code.
        std::regex regHttpVersionCode("HTTP/(\\d.\\d) (\\d\\d\\d) ([\\w ]*)\r\n");
        std::cmatch result;
        std::regex_search(message.c_str(), result, regHttpVersionCode);

        // Parse the HTTP version
        std::string strHttpVersion(result.str(1));
        if (strHttpVersion == "1.1")
            mHttpVersion = HttpVersion::HTTP1_1;
        else if (strHttpVersion == "1.0")
            mHttpVersion = HttpVersion::HTTP1_0;
        else if (strHttpVersion == "2.0")
            mHttpVersion = HttpVersion::HTTP2_0;
        else
            mHttpVersion = HttpVersion::UNKNOWN;

        // Parse the code.
        std::stringstream codeStream(result.str(2));
        if (!(codeStream >> mCode))
            err::exit(err::Error::PARAM, "The HTTP code is not convertible!");
        mCodeMessage = result.str(3);

        // Get the content location.
        std::regex regLocation("Location: (.*)\r\n");
        std::regex_search(message.c_str(), result, regLocation);
        mLocation = result.str(1);

        // Check, if the message is chunked.
        //std::regex regChunked("Transfer-Encoding: chunked\r\n");
        std::regex regChunked("Transfer-Encoding: chunked");
        if (std::regex_search(message.c_str(), regChunked))
            mChunked = true;

        // Get the content type.
        std::regex regContentType("Content-Type: ([\\w/])(:?;|\r\n)");
        std::regex_search(message.c_str(), result, regContentType);
        std::string strContentType = result.str(1);
        if (strContentType == "text/html")
            mContentType = HttpContentType::TEXT_HTML;
        else
            mContentType = HttpContentType::UNKNOWN;
    }

    util::HttpVersion HttpParser::getVersion() const
    {
        return mHttpVersion;
    }

    std::string HttpParser::getVersionString() const
    {
        switch (mHttpVersion)
        {
            case HttpVersion::HTTP1_0:
                return std::string("HTTP/1.0");
            case HttpVersion::HTTP1_1:
                return std::string("HTTP/1.0");
            case HttpVersion::HTTP2_0:
                return std::string("HTTP/1.0");
            case HttpVersion::UNKNOWN:
                return std::string("UNKNOWN");
        }
    }

    int HttpParser::getCode() const
    {
        return mCode;
    }

    std::string HttpParser::getCodeMessage() const
    {
        return mCodeMessage;
    }

    std::string HttpParser::getLocation() const
    {
        return mLocation;
    }

    bool HttpParser::isChunked() const
    {
        return mChunked;
    }

    util::HttpContentType HttpParser::getType() const
    {
        return mContentType;
    }

    std::string HttpParser::getTypeString() const
    {
        switch (mContentType)
        {
            case HttpContentType::TEXT_HTML:
                return std::string("text/html");
            case HttpContentType::UNKNOWN:
                return std::string("unknown");
        }
    }

    size_t HttpParser::getContentLength() const
    {
        return mContentLength;
    }
}
