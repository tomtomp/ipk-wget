/*
 * Task IPK - webclient: Download client in C++ for IPK 2015/2016
 * Name and surname: Tomáš Polášek
 * Login: xpolas34
 */

#ifndef HOSTINFO_H
#define HOSTINFO_H

#include <string>

#include "NetworkIncludes.h"
#include "UrlParser.h"
#include "ErrorHandling.h"

namespace net
{
    class HostInfo {
    public:
        HostInfo() = delete;

        /**
         * Get the information about host.
         */
        HostInfo(const util::UrlParser &parser);

        std::string getHostName() const;
        const hostent *getHostInfo() const;
        std::string getIpString() const;
    private:
        std::string mHostName;
        hostent *mpHostInfo;
        std::string mIpString;
    protected:
    };
}


#endif //HOSTINFO_H
