/*
 * Task IPK - webclient: Download client in C++ for IPK 2015/2016
 * Name and surname: Tomáš Polášek
 * Login: xpolas34
 */

#include "TcpConnection.h"

namespace net
{
    TcpConnection::TcpConnection(const net::HostInfo &hostInfo,
                                 const util::UrlParser &parser) :
        mConnected(false),
        mPort(parser.getPort())
    {
        bzero(&mAddress, sizeof(mAddress));
        const hostent *serverInfo = hostInfo.getHostInfo();
        memcpy(reinterpret_cast<char*>(&mAddress.sin_addr),
               *serverInfo->h_addr_list,
               static_cast<size_t>(serverInfo->h_length));
        mAddress.sin_family = AF_INET;
        mAddress.sin_port = htons(static_cast<uint16_t>(mPort));
    }

    void TcpConnection::connect(const net::TcpSocket &socket)
    {
        if (::connect(socket.getSocket(),
                      reinterpret_cast<sockaddr*>(&mAddress),
                      sizeof(mAddress)) != 0)
            err::exit(err::Error::NETWORK, "Unable to connect to the server!");
        mConnected = true;
    }

    bool TcpConnection::isConnected() const
    {
        return mConnected;
    }
}
