/*
 * Task IPK - webclient: Download client in C++ for IPK 2015/2016
 * Name and surname: Tomáš Polášek
 * Login: xpolas34
 */

#include "TcpReceiver.h"

namespace net
{
    TcpReceiver::TcpReceiver(const net::TcpSocket &socket) :
        mSocket(socket.getSocket())
    {

    }

    std::string TcpReceiver::receiveMessage()
    {
        clearBuffer();
        ssize_t received = 0;
        if ((received = recv(mSocket, mBuffer, BUFFER_SIZE, 0)) < 0)
            err::exit(err::Error::NETWORK, "Unable to receive the message!");

        return std::string(mBuffer, mBuffer + received);
    }

    void TcpReceiver::clearBuffer()
    {
        bzero(mBuffer, BUFFER_SIZE + 1);
    }
}
