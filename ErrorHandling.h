/*
 * Task IPK - webclient: Download client in C++ for IPK 2015/2016
 * Name and surname: Tomáš Polášek
 * Login: xpolas34
 */

#ifndef ERRORHANDLING_H
#define ERRORHANDLING_H

#include <iostream>
#include <string>
#include <cstdlib>
#include <cstdio>
#include <cerrno>

namespace err
{
    enum class Error
    {
        OK = 0,
        PARAM,
        NETWORK,
        MESSAGE,
        OUTPUT,
        UNKNOWN
    };

    /**
     * Exits the program with the correct exit code.
     *
     * @param code Error code to exit with.
     * @param message Message to print out.
     */
    void exit(Error code, const std::string &message);
}

#endif //ERRORHANDLING_H
