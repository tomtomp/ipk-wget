/*
 * Task IPK - webclient: Download client in C++ for IPK 2015/2016
 * Name and surname: Tomáš Polášek
 * Login: xpolas34
 */

#include "main.h"

int main(int argc, char *argv[])
{
    if (argc != 2)
        err::exit(err::Error::PARAM, "The correct number of parameters is 2!");

    std::string inputUrl = argv[1];

    app::HttpReceiver receiver(inputUrl);

    return 0;
}