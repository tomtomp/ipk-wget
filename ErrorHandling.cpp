/*
 * Task IPK - webclient: Download client in C++ for IPK 2015/2016
 * Name and surname: Tomáš Polášek
 * Login: xpolas34
 */

#include "ErrorHandling.h"

namespace err
{
    void exit(Error code, const std::string &message)
    {
        std::cerr << "Error : " << message << std::endl << std::flush;
        std::exit(static_cast<int>(code));
    }
}

