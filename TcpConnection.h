/*
 * Task IPK - webclient: Download client in C++ for IPK 2015/2016
 * Name and surname: Tomáš Polášek
 * Login: xpolas34
 */

#ifndef TCPCONNECTION_H
#define TCPCONNECTION_H

#include "NetworkIncludes.h"
#include "ErrorHandling.h"
#include "HostInfo.h"
#include "TcpSocket.h"
#include "TcpSocket.h"

namespace net
{
    /**
     * Starts and stores information about a single TCP connection.
     */
    class TcpConnection
    {
    public:
        TcpConnection() = delete;

        TcpConnection(const net::HostInfo &hostInfo,
                      const util::UrlParser &parser);

        /**
         * Connect using the information passed in constructor + the socket.
         *
         * @param socket Socket to make the connection with.
         */
        void connect(const net::TcpSocket &socket);
        bool isConnected() const;
    private:
        bool mConnected;
        sockaddr_in mAddress;
        int mPort;
    protected:
    };
}


#endif //TCPCONNECTION_H
