/*
 * Task IPK - webclient: Download client in C++ for IPK 2015/2016
 * Name and surname: Tomáš Polášek
 * Login: xpolas34
 */

#ifndef HTTPPARSER_H
#define HTTPPARSER_H

#include <string>
#include <regex>

#include "ErrorHandling.h"

#include "TcpReceiver.h"

namespace util
{
    enum class HttpVersion
    {
        HTTP1_0 = 0,
        HTTP1_1,
        HTTP2_0,
        UNKNOWN
    };

    enum class HttpContentType
    {
        TEXT_HTML = 0,
        UNKNOWN
    };

    /**
     * Used for parsing of the first HTTP message.
     */
    class HttpParser
    {
    public:
        /**
         * Take the first received message and get HTTP information.
         *
         * @param message The first received message.
         */
        HttpParser(const std::string &message);

        /**
         * Parse the given message.
         *
         * @param message The message to parse.
         */
        void parse(const std::string &message);

        util::HttpVersion       getVersion() const;
        std::string             getVersionString() const;
        int                     getCode() const;
        std::string             getCodeMessage() const;
        std::string             getLocation() const;
        bool                    isChunked() const;
        util::HttpContentType   getType() const;
        std::string             getTypeString() const;
        size_t                  getContentLength() const;
    private:
        util::HttpVersion       mHttpVersion;
        int                     mCode;
        std::string             mCodeMessage;
        std::string             mLocation;
        bool                    mChunked;
        util::HttpContentType   mContentType;
        size_t                  mContentLength;

        size_t                  mCurrDataSize;

    protected:
    };
}


#endif //HTTPPARSER_H
