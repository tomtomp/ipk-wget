/*
 * Task IPK - webclient: Download client in C++ for IPK 2015/2016
 * Name and surname: Tomáš Polášek
 * Login: xpolas34
 */

#ifndef TCPSOCKET_H
#define TCPSOCKET_H

#include "NetworkIncludes.h"
#include "ErrorHandling.h"

namespace net
{
    /**
     * Starts and stores information about a single TCP socket.
     */
    class TcpSocket
    {
    public:
        /**
         * Create new socket.
         */
        TcpSocket();

        int getSocket() const;

        ~TcpSocket();
    private:
        int mSocketNum;
    protected:
    };
}


#endif //TCPSOCKET_H
