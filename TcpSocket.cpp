/*
 * Task IPK - webclient: Download client in C++ for IPK 2015/2016
 * Name and surname: Tomáš Polášek
 * Login: xpolas34
 */

#include "TcpSocket.h"

namespace net
{
    TcpSocket::TcpSocket() :
        mSocketNum(0)
    {
        if ((mSocketNum = socket(AF_INET, SOCK_STREAM, 0)) <= 0)
            err::exit(err::Error::NETWORK, "Unable to open client socket!");
    }

    int TcpSocket::getSocket() const
    {
        return mSocketNum;
    }

    TcpSocket::~TcpSocket()
    {
        close(mSocketNum);
    }
}
