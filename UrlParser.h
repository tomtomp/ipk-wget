/*
 * Task IPK - webclient: Download client in C++ for IPK 2015/2016
 * Name and surname: Tomáš Polášek
 * Login: xpolas34
 */

#ifndef URLPARSER_H
#define URLPARSER_H

#include <string>
#include <regex>

#include "ErrorHandling.h"

namespace util
{
    /**
     * Used for parsing URL.
     */
    class UrlParser
    {
    public:
        UrlParser() = delete;

        /**
         * Parse the input URL.
         * @param inputUrl URL to parse.
         */
        UrlParser(const std::string &inputUrl);

        std::string getUrl() const;
        std::string getServer() const;
        unsigned int getPort() const;
        std::string getPath() const;
        std::string getObject() const;
        std::string getProtocol() const;
    private:
        std::string mInputUrl;
        std::string mServer;
        unsigned int mPort;
        std::string mPath;
        std::string mObject;
        std::string mProtocol;
    protected:
    };
}


#endif //URLPARSER_H
