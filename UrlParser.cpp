/*
 * Task IPK - webclient: Download client in C++ for IPK 2015/2016
 * Name and surname: Tomáš Polášek
 * Login: xpolas34
 */

#include "UrlParser.h"

namespace util
{
    UrlParser::UrlParser(const std::string &inputUrl) :
        mInputUrl(inputUrl),
        mPort(0)
    {
        std::smatch match;
        std::regex urlRegex("^([a-z]+://)?([\\d\\w\\.-]+(?:\\.[\\w\\.]{2,6}))(?::(\\d+))?"
                            "([\\w\\\\ \\./?_&=-]*/([\\w\\\\ \\.?_&=-]*))?$",
                            std::regex_constants::icase);

        if (! std::regex_search(inputUrl, match, urlRegex))
            err::exit(err::Error::PARAM, "The parameter is not a correct URL!");

        // Match should contain the captured groups :
        //   [0] => The whole URL.
        //   [1] => The protocol.
        //   [2] => The server part of the URL.
        //   [3] => The port.
        //   [4] => The object path part of the URL.
        //   [5] => The object part of the URL.

        mProtocol = static_cast<std::string>(match[1]);

        mServer = static_cast<std::string>(match[2]);

        std::string strPort = static_cast<std::string>(match[3]);
        // Default port is 80.
        int port = 80;
        if (!strPort.empty())
        {
            // Convert the string to integer.
            std::stringstream portStream(strPort);
            if (!(portStream >> port))
                err::exit(err::Error::PARAM, "The port is not correct!");
            if (port <= 0)
                err::exit(err::Error::PARAM, "The port has to be bigger than 0!");
        }
        mPort = static_cast<unsigned int>(port);

        mPath = static_cast<std::string>(match[4]);
        if (mPath.empty())
        {
            // Default path is "/".
            mPath = "/";
        }
        else
        {
            // Replace escaped spaces with %20
            mPath = std::regex_replace(mPath, std::regex("\\ "), "%20");
        }

        mObject = static_cast<std::string>(match[5]);
        if (mObject.empty())
        {
            // Default object name is "index.html"
            mObject = "index.html";
        }
        else
        {
            // Replace escaped spaces with %20
            mObject = std::regex_replace(mObject, std::regex("\\ "), "%20");
        }
    }

    std::string UrlParser::getUrl() const
    {
        return mInputUrl;
    }

    std::string UrlParser::getServer() const
    {
        return mServer;
    }

    unsigned int UrlParser::getPort() const
    {
        return mPort;
    }

    std::string UrlParser::getPath() const
    {
        return mPath;
    }

    std::string UrlParser::getObject() const
    {
        return mObject;
    }

    std::string UrlParser::getProtocol() const
    {
        return mProtocol;
    }
}
