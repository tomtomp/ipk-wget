/*
 * Task IPK - webclient: Download client in C++ for IPK 2015/2016
 * Name and surname: Tomáš Polášek
 * Login: xpolas34
 */

#ifndef TCPSENDER_H
#define TCPSENDER_H

#include <string>

#include "NetworkIncludes.h"

#include "TcpSocket.h"

namespace net
{
    /**
     * Used for sending messages.
     */
    class TcpSender
    {
    public:
        TcpSender() = delete;

        TcpSender(const net::TcpSocket &socket);

        void sendMessage(const std::string &message);
    private:
        int mSocket;
    protected:
    };
}


#endif //TCPSENDER_H
