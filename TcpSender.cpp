/*
 * Task IPK - webclient: Download client in C++ for IPK 2015/2016
 * Name and surname: Tomáš Polášek
 * Login: xpolas34
 */

#include "TcpSender.h"

namespace net
{
    TcpSender::TcpSender(const net::TcpSocket &socket) :
        mSocket(socket.getSocket())
    {

    }

    void TcpSender::sendMessage(const std::string &message)
    {
        const char *cMessage = message.c_str();
        std::string::size_type size = message.length();
        if (send(mSocket, cMessage, size, 0) < size)
            err::exit(err::Error::NETWORK, "Unable to send the whole message!");
    }
}
